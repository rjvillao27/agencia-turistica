/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import agenciaturistica_clicktours.AgenciaTuristica_ClickTours;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.clientes;
import agenciaturistica_clicktours.Cliente;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
/**
 *
 * @author anthony
 */
public class VentanaRegistro {
    public static BorderPane root= new BorderPane();
    private GridPane registro= new GridPane();
    private Label titulo= new Label("VENTANA DE REGISTRO");
    private Button b= new Button("Guardar y Continuar");
    private Button consultar= new Button ("Consultar registros");
    private Pane estilo= new Pane();
    private VBox reg= new VBox();
    
    TextField c= new TextField();
    TextField n= new TextField();
    TextField d= new TextField();
    TextField e= new TextField();
    
    Label ce=new Label("Cedula: ");
    Label no=new Label("Nombre: ");
    Label di=new Label("Direccion: ");
    Label em=new Label("E-mail: ");
    
    
    
    public VentanaRegistro(){
        generarVentana();
    }
    
    
    
    public void generarVentana(){
        consultar.setOnAction(e->agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new ConsultarRegistros().getRoot()));
        ce.setFont(new Font("Arial",20));
        no.setFont(new Font("Arial",20));
        di.setFont(new Font("Arial",20));
        em.setFont(new Font("Arial",20));
        
        
        registro.add(ce, 0, 0);
        registro.add(no, 0, 1);
        registro.add(di, 0, 2);
        registro.add(em, 0, 3);
        
        registro.add(c, 1, 0);
        registro.add(n, 1, 1);
        registro.add(d, 1, 2);
        registro.add(e, 1, 3);
        
        
        
        registro.setVgap(5);
        root.setTop(titulo);
        estilo.setStyle( "-fx-background-image:url('/recursos/hotel.jpg');"+
                         "-fx-background-repeat: stretch;"+
                         "-fx-background-position: center center;");
        reg.getChildren().add(registro);
        estilo.getChildren().add(reg);
        reg.setLayoutX(25);
        reg.setLayoutY(30);
        HBox cont= new HBox(350);
        b.setMaxSize(200, 250);
        b.setStyle("-fx-base: blue;");
        consultar.setMaxSize(200, 250);
        consultar.setStyle("-fx-base: blue;");
        cont.getChildren().addAll(consultar,b);
        root.setCenter(estilo);
        root.setBottom(cont);

        root.setAlignment(titulo, Pos.CENTER);
        root.setAlignment(reg, Pos.CENTER);
        root.setAlignment(b, Pos.CENTER);
        
        b.setOnMouseClicked(ex -> {
            if(guardarDatos(n,c,d,e)){
                agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new OpcionBusqueda().getRoot());
        }
        
     });
    }
    private boolean guardarDatos(TextField nombre, TextField cedula, TextField direccion, TextField email){
        try {
            int numC=Integer.parseInt(cedula.getText());
            String numCedula = cedula.getText();
            String name="";
            String direc="";
            String em="";
            if(nombre.getText().length()!=0 && direccion.getText().length()!=0 && email.getText().length()!=0){
                name = nombre.getText();
                direc = direccion.getText();
                em = email.getText();
            }else{
                String k="a";
                int numk=Integer.parseInt(k);
            }
            clientes.add(new Cliente(numCedula, name, direc, em));
            
            System.out.println(clientes);
            
            n.clear();
            c.clear();
            d.clear();
            e.clear();
            
            AgenciaTuristica_ClickTours.select=new Cliente(numCedula, name, direc, em);
            
            
        } catch (NumberFormatException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error al guardar los datos");
            a.setHeaderText("Revise los datos ingresados");
            a.setContentText("Al parecer ingreso letras en la cédula o dejo espacios en blanco");
            a.showAndWait();
            return false;
        }
        return true;
    }
    
    public BorderPane getRoot() {
        return root;
    }
    
    
}
