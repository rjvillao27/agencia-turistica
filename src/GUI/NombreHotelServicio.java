/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import agenciaturistica_clicktours.AgenciaTuristica_ClickTours;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.ciudades;
import agenciaturistica_clicktours.Catalogo;
import agenciaturistica_clicktours.Ciudad;
import agenciaturistica_clicktours.Hotel;
import agenciaturistica_clicktours.Servicio;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author josue
 */
public class NombreHotelServicio {
    private BorderPane root= new BorderPane();
    private Pane fondo= new Pane();
    private VBox organizar= new VBox();
    private HBox opcionesBusqueda= new HBox();
    private HBox opcionesBusqueda2= new HBox();
    private Label nombreHotel= new Label("Nombre Hotel: ");
    private Label servi= new Label("Servicio: ");
    private TextField nomHot= new TextField();
    private TextField servicio= new TextField();
    private Button buscar= new Button("Buscar");
    private HBox botones=new HBox();
    private Button a = new Button("Atras");
    private ScrollPane hoteles= new ScrollPane();
    private VBox hotelesVerticales= new VBox();
    private HBox contenedorScroll= new HBox();
    private Ciudad defecto=null;
    private List<Ciudad> ciud=new ArrayList<>();
    private List<Ciudad> remover=new ArrayList<>();
    
    public NombreHotelServicio(){
        generarVentana();
        
    }
    
    
    public void generarVentana(){
        opcionesBusqueda.getChildren().addAll(nombreHotel,nomHot);
        opcionesBusqueda2.getChildren().addAll(servi,servicio);
        organizar.getChildren().addAll(opcionesBusqueda,opcionesBusqueda2,buscar,contenedorScroll);
        
        fondo.setStyle( "-fx-background-image:url('/recursos/Suite.jpg');"+
                         "-fx-background-repeat: stretch;"+
                         "-fx-background-position: center center;");
        fondo.getChildren().add(organizar);
        botones.getChildren().addAll(a);
        botones.setAlignment(Pos.CENTER);
        botones.setSpacing(15);
        organizar.setSpacing(10);
        opcionesBusqueda.setSpacing(15);
        opcionesBusqueda2.setSpacing(15);
        organizar.setLayoutX(40);
        organizar.setLayoutY(20);
        
        nombreHotel.setFont(new Font("Arial",17));
        servi.setFont(new Font("Arial",17));
        
        a.setMaxSize(100, 200);
        a.setStyle("-fx-base: blue;");

        
        root.setCenter(fondo);
        root.setBottom(botones);
        
        a.setOnMouseClicked(e-> AgenciaTuristica_ClickTours.s.setRoot(new OpcionBusqueda().getRoot()));
        
        hoteles.setContent(hotelesVerticales);
        hoteles.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        

        buscar.setOnAction(e-> buscarHoteles());
        
    }
    
    public void buscarHoteles(){               
        if(nomHot.getText()!=null && servicio.getText()!=null){
            
        hoteles.fitToWidthProperty().set(true);
        contenedorScroll.setVisible(true);
        
        contenedorScroll.setPrefWidth(1000);
        contenedorScroll.setPrefHeight(450);
        contenedorScroll.getChildren().clear();
        contenedorScroll.getChildren().add(hoteles);
        
        hotelesVerticales.getChildren().clear();    
        String nombre=nomHot.getText();
        String ser=servicio.getText();
        nomHot.setText(null);
        servicio.setText(null); 
        for (Hotel h:AgenciaTuristica_ClickTours.hoteles){ 
            if(nombre.equals(h.getNombre())){           
                Image image = new Image(h.getFotoHotel());
                ImageView img=new ImageView(image);
                img.setFitHeight(80);
                img.setFitWidth(80);
                HBox hotelEncontrado= new HBox();
                VBox datosH= new VBox();
                HBox bot=new HBox();
                Button info=new Button("Mas informacion");
                Button mapa=new Button("Ver Mapa");
                Button reservar=new Button("Reservar");
                bot.getChildren().addAll(info,mapa,reservar);
                datosH.setSpacing(17);
                datosH.getChildren().addAll(new Label(h.getNombre()),new Label(h.getDireccionHotel()),bot);
                hotelEncontrado.getChildren().addAll(img,datosH);
                hotelEncontrado.setSpacing(20);
                hotelesVerticales.setSpacing(50);
                hotelesVerticales.getChildren().add(hotelEncontrado);
                


                VBox right=new VBox();
                right.setSpacing(20);
                TextArea t=new TextArea("INFORMACION DEL HOTEL: \n"+h.getDescripcionHotel());
                t.setWrapText(true); 

                right.getChildren().add(t);
                info.setOnMouseClicked(e-> root.setRight(right));
                reservar.setOnAction(e->reservarH(h));
                mapa.setOnAction(e-> buscarHotel(h)); 
            }
        }
        
        
        for (Hotel h:AgenciaTuristica_ClickTours.hoteles){ 
                for(Servicio s:AgenciaTuristica_ClickTours.servicios){
                    if(s.getIdHotel().equals(h.getId())){
                        for (Catalogo c: AgenciaTuristica_ClickTours.catalogos){
                            if(c.getId().equals(s.getIdServicio()) && c.getNombre().toLowerCase().equals(ser.toLowerCase())){
                                Image image = new Image(h.getFotoHotel());
                                ImageView img=new ImageView(image);
                                img.setFitHeight(80);
                                img.setFitWidth(80);
                                HBox hotelEncontrado= new HBox();
                                VBox datosH= new VBox();
                                HBox bot=new HBox();
                                Button info=new Button("Mas informacion");
                                Button mapa=new Button("Ver Mapa");
                                Button reservar=new Button("Reservar");
                                bot.getChildren().addAll(info,mapa,reservar);
                                datosH.setSpacing(17);
                                datosH.getChildren().addAll(new Label(h.getNombre()),new Label(h.getDireccionHotel()),bot);
                                hotelEncontrado.getChildren().addAll(img,datosH);
                                hotelEncontrado.setSpacing(20);
                                hotelesVerticales.setSpacing(50);
                                hotelesVerticales.getChildren().add(hotelEncontrado);


                                VBox right=new VBox();
                                right.setSpacing(20);
                                TextArea t=new TextArea("INFORMACION DEL HOTEL: \n"+h.getDescripcionHotel());
                                t.setWrapText(true); 

                                right.getChildren().add(t);
                                info.setOnMouseClicked(e-> root.setRight(right));
                                reservar.setOnAction(e->reservarH(h));
                                mapa.setOnAction(e-> buscarHotel(h)); 
                            }
                        }
                    }

                }
        }
        
        
        }
    }
    
        public void reservarH(Hotel h){
            AgenciaTuristica_ClickTours.sleccionado=h;
            AgenciaTuristica_ClickTours.s.setRoot(new Reserva().getRoot());
           
        }
    
        public void buscarHotel(Hotel h){
         URI myURI;
         try {
             String nom= h.getNombre();
             String[] list= nom.split(" ");
             String query="";
             for(String i:list){
                 query=query+i;
                 query=query+"%20";                
             }
             String id=h.getIdCiudad();
             String nombre="";
             for(Ciudad c:ciudades){
                 if(id.equals(c.getId())){
                     nombre=c.getNombre();
                 }
             }
             query=query+nombre;
             myURI = new URI("https://www.google.com/maps/search/?api=1&query="+query);
             Desktop.getDesktop().browse(myURI);
     
         } catch (URISyntaxException ex) {
             Logger.getLogger(BusquedaHoteles.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(BusquedaHoteles.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public BorderPane getRoot(){
        return root;
    }
    
}
