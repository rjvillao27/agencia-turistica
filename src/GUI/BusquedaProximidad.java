/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import agenciaturistica_clicktours.AgenciaTuristica_ClickTours;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.ciudades;
import agenciaturistica_clicktours.Catalogo;
import agenciaturistica_clicktours.Ciudad;
import agenciaturistica_clicktours.Hotel;
import agenciaturistica_clicktours.Servicio;
import generadorhtmlmapbox.GeneradorHTMLMapBox;
import generadorhtmlmapbox.Ubicacion;
import geocoding.GeoCoding;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author josue
 */
public class BusquedaProximidad {
    private BorderPane root= new BorderPane();
    private Pane fondo= new Pane();
    private VBox organizar= new VBox();
    private HBox opcionesBusqueda= new HBox();
    private Label nombSit= new Label("Ingrese su destino turistico: ");
    private TextField nombreSitio= new TextField();
    private Button buscar= new Button("Buscar");
    private HBox botones=new HBox();
    private Button a = new Button("Atras");
    private ScrollPane hoteles= new ScrollPane();
    private VBox hotelesVerticales= new VBox();
    private HBox contenedorScroll= new HBox();
    private Ciudad defecto=null;
    private List<Ciudad> ciud=new ArrayList<>();
    private List<Ciudad> remover=new ArrayList<>();
    
    public BusquedaProximidad(){
        generarVentana();
        
    }
    
    
    public double haversine(double lat1, double long1, double lat2, double long2){  
        int R_ecuatorialKM =  6378;
        double Variacion_lat = lat2-lat1;
        double Variacion_long = long2-long1;
        double a = (Math.sin(Variacion_lat/2)* Math.sin(Variacion_lat/2)) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(Variacion_long/2)*Math.sin(Variacion_long/2);
        double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R_ecuatorialKM * c;
        return d ;
    }
    
    public void generarVentana(){
        opcionesBusqueda.getChildren().addAll(nombSit,nombreSitio);
        organizar.getChildren().addAll(opcionesBusqueda,buscar,contenedorScroll);
        
        fondo.setStyle( "-fx-background-image:url('/recursos/Suite.jpg');"+
                         "-fx-background-repeat: stretch;"+
                         "-fx-background-position: center center;");
        fondo.getChildren().add(organizar);
        botones.getChildren().addAll(a);
        botones.setAlignment(Pos.CENTER);
        botones.setSpacing(15);
        organizar.setSpacing(10);
        opcionesBusqueda.setSpacing(15);
        organizar.setLayoutX(40);
        organizar.setLayoutY(20);
        
        nombSit.setFont(new Font("Arial",17));
        
        
        a.setMaxSize(100, 200);
        a.setStyle("-fx-base: blue;");
       
        
        root.setCenter(fondo);
        root.setBottom(botones);
        
        a.setOnMouseClicked(e-> AgenciaTuristica_ClickTours.s.setRoot(new OpcionBusqueda().getRoot()));
        
        hoteles.setContent(hotelesVerticales);
        hoteles.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        
        buscar.setOnAction(e-> buscarHoteles());
        
    }
    
    public void buscarHoteles(){
        ArrayList<Hotel> topCercanos=new ArrayList<>();
        ArrayList<Hotel> hotelesC=new ArrayList<>();
        if(nombreSitio.getText().length()!=0){
            hoteles.fitToWidthProperty().set(true);
            contenedorScroll.setVisible(true);
            
            contenedorScroll.setPrefWidth(1000);
            contenedorScroll.setPrefHeight(450);
            contenedorScroll.getChildren().clear();
            contenedorScroll.getChildren().add(hoteles);


            hotelesVerticales.getChildren().clear();
            
            GeoCoding a = new GeoCoding("pk.eyJ1IjoiYmxvZWsyNyIsImEiOiJjazY1emtpdGoxZXplM2xtbTRrczVqYTVpIn0.K15XLqmPhZX2wqxwHxgJ1g");
            Ubicacion u = a.consultar(nombreSitio.getText()); 
            nombreSitio.setText("");
            HashMap topHoteles=new HashMap<>();
            
            for (Hotel h:AgenciaTuristica_ClickTours.hoteles){ 
                double longi=h.getLongitud();
                double latid=h.getLatitud();
                double distancia=haversine(u.getLatitud(),u.getLongitud(),latid,longi);
                topHoteles.put(distancia, h);
            }
            
            
            TreeMap topHoteless=new TreeMap(topHoteles);
            
            topHoteless.forEach((k,v) -> hotelesC.add((Hotel) v));
            
            topCercanos.add(hotelesC.get(0));
            topCercanos.add(hotelesC.get(1));
            topCercanos.add(hotelesC.get(2));
            topCercanos.add(hotelesC.get(3));
            topCercanos.add(hotelesC.get(4));
            for (Hotel h:topCercanos){
                Image image = new Image(h.getFotoHotel());
                ImageView img=new ImageView(image);
                img.setFitHeight(80);
                img.setFitWidth(80);
                HBox hotelEncontrado= new HBox();
                VBox datosH= new VBox();
                HBox bot=new HBox();
                Button info=new Button("Mas informacion");
                Button mapa=new Button("Ver Mapa");
                Button reservar=new Button("Reservar");
                bot.getChildren().addAll(info,mapa,reservar);
                datosH.setSpacing(17);
                datosH.getChildren().addAll(new Label(h.getNombre()),new Label(h.getDireccionHotel()),bot);
                hotelEncontrado.getChildren().addAll(img,datosH);
                hotelEncontrado.setSpacing(20);
                hotelesVerticales.setSpacing(50);
                hotelesVerticales.getChildren().add(hotelEncontrado);


                VBox right=new VBox();
                right.setSpacing(20);
                TextArea t=new TextArea("INFORMACION DEL HOTEL: \n"+h.getDescripcionHotel());
                t.setWrapText(true); 

                right.getChildren().add(t);
                info.setOnMouseClicked(e-> root.setRight(right));
                reservar.setOnAction(e->reservarH(h));
                mapa.setOnAction(e-> buscarHotel(h)); 
            } 
            
            try { 
                GeneradorHTMLMapBox b = new GeneradorHTMLMapBox("pk.eyJ1IjoiYmxvZWsyNyIsImEiOiJjazY1dWN4czgxMWI5M2txanprNzhyMW02In0.QHoG4El2yyz7HWwQ7YDc-Q");
                b.establecerLocacionInicial(u.getLongitud(), u.getLatitud());
                for(Hotel h:topCercanos){
                    b.anadirMarcador(h.getLongitud(),h.getLatitud(), h.getNombre());
                }

                b.grabarHTML("src/HTML/mapaHotelesCercanos.html");
                Desktop.getDesktop().browse(Paths.get("src/HTML/mapaHotelesCercanos.html").toAbsolutePath().toUri());
           
            } catch (IOException ex) {
                Logger.getLogger(BusquedaProximidad.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
        public void reservarH(Hotel h){
            AgenciaTuristica_ClickTours.sleccionado=h;
            AgenciaTuristica_ClickTours.s.setRoot(new Reserva().getRoot());
            
        }
    
        public void buscarHotel(Hotel h){
         URI myURI;
         try {
             String nom= h.getNombre();
             String[] list= nom.split(" ");
             String query="";
             for(String i:list){
                 query=query+i;
                 query=query+"%20";                
             }
             String id=h.getIdCiudad();
             String nombre="";
             for(Ciudad c:ciudades){
                 if(id.equals(c.getId())){
                     nombre=c.getNombre();
                 }
             }
             query=query+nombre;
             myURI = new URI("https://www.google.com/maps/search/?api=1&query="+query);
             Desktop.getDesktop().browse(myURI);
     
         } catch (URISyntaxException ex) {
             Logger.getLogger(BusquedaHoteles.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(BusquedaHoteles.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public BorderPane getRoot(){
        return root;
    }
}
