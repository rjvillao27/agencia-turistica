/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import agenciaturistica_clicktours.AgenciaTuristica_ClickTours;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.clientes;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.habitaciones;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.hoteles;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.select;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.sleccionado;
import agenciaturistica_clicktours.Habitacion;
import agenciaturistica_clicktours.ReservaRealizada;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;



/**
 *
 * @author josue
 */
public class Reserva {
    private BorderPane root=new BorderPane();
    private Button elegirOtro=new Button("Cambiar de hotel");

    private HBox op1= new HBox();
    private HBox op2= new HBox();
    private HBox op3= new HBox();
    private HBox op4= new HBox();
    private HBox op5= new HBox();

    private ComboBox cboHabit=new ComboBox();
    private DatePicker fechaReserv = new DatePicker();
    private DatePicker fechaFin = new DatePicker();
    private Button guardarSalir=new Button("Guardar y Continuar");
    
    public Reserva(){
        generarVentana();
        llenarHabitacion();
    }
    
    
    public void generarVentana(){
        
        
      
        VBox cont=new VBox(15);
        
        
        
        fechaReserv.setValue(LocalDate.now()); 
        fechaReserv.setEditable(false);
        fechaFin.setEditable(true); 
        op1.getChildren().add(new Label("El cliente para hacer el registro es: "+ select));
        op2.getChildren().add(new Label("El hotel que ha seleccionado ha sido: " + sleccionado));
        op3.getChildren().addAll(new Label("Elija la habitacion que desee: "),cboHabit);
        op4.getChildren().addAll(new Label("Escoja la fecha para la que desea reservar: "),fechaReserv);
        op5.getChildren().addAll(new Label("Escoja la fecha del fin de la reservar "),fechaFin);
        
        
        HBox botones=new HBox(15);
        botones.getChildren().addAll(guardarSalir,elegirOtro);
        botones.setAlignment(Pos.BOTTOM_CENTER);
        cont.setAlignment(Pos.TOP_CENTER);
        cont.getChildren().addAll(op1,op2,op3,op4,op5);
        
        root.setCenter(cont);
        root.setBottom(botones);
     
        
        
  
        
        elegirOtro.setOnAction(e->agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new OpcionBusqueda().getRoot()));       
        guardarSalir.setOnAction(e->terminar());
    }
    
    public void terminar(){
        ReservaRealizada r= new ReservaRealizada(select,sleccionado, (Habitacion) cboHabit.getValue(),fechaReserv.getValue(),fechaFin.getValue());
        AgenciaTuristica_ClickTours.reservasReal.add(r);
        try(ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("src/recursos/reservasHechas.dat"))){
            o.writeObject(AgenciaTuristica_ClickTours.reservasReal);
        }
        catch(IOException e){
            System.out.println(e);
        }
        Habitacion habit= (Habitacion) cboHabit.getValue();
        
        AgenciaTuristica_ClickTours.s.setRoot(VentanaRegistro.root);
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setTitle("Confirmacion de reserva");
        a.setHeaderText("El valor a pagar sera: "+ habit.getTarifaSencilla());
        a.setContentText(":)");
        a.showAndWait();
       
    }
    
    public void llenarHabitacion(){
        ArrayList<Habitacion> disponibles=new ArrayList<>();
        String id=sleccionado.getId();
        for(Habitacion hab:habitaciones){
            String idh=hab.getIdHotel();
            if(idh.equals(id)){
                disponibles.add(hab);
            }
        }
        cboHabit.getItems().addAll(disponibles);
    }
    
    public BorderPane getRoot() {
        return root;
    }
    
    
}
