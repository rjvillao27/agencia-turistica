/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static GUI.VentanaRegistro.root;
import static agenciaturistica_clicktours.AgenciaTuristica_ClickTours.reservasReal;
import agenciaturistica_clicktours.Cliente;
import agenciaturistica_clicktours.Habitacion;
import agenciaturistica_clicktours.Hotel;
import agenciaturistica_clicktours.ReservaRealizada;
import java.time.LocalDate;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author josue
 */
public class ConsultarRegistros {
    public static BorderPane root= new BorderPane();

    
    private Button b= new Button("Atras");
    private Pane estilo= new Pane();
    private VBox reg= new VBox();
    private ComboBox cboOpciones=new ComboBox();
    private TextArea muestraDatos= new TextArea();
    private Label texto= new Label("Consultar por:");
    private Button confirm= new Button("Consultar");
    public ConsultarRegistros(){
        generarVentana();
    }
    
    public void generarVentana(){
        llenarComboOpciones();
        b.setOnAction(e->agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new VentanaRegistro().getRoot()));
        
        estilo.setStyle( "-fx-background-image:url('/recursos/hotel.jpg');"+
                         "-fx-background-repeat: stretch;"+
                         "-fx-background-position: center center;");
        reg.getChildren().addAll(texto,cboOpciones,confirm);
        estilo.getChildren().add(reg);
        reg.setLayoutX(25);
        reg.setLayoutY(30);
        
        b.setMaxSize(200, 250);
        b.setStyle("-fx-base: blue;");
        
       
        root.setCenter(estilo);
        root.setBottom(b);

        
        root.setAlignment(reg, Pos.CENTER);
        root.setAlignment(b, Pos.CENTER);
        confirm.setOnAction(ex->mostrarDatos());
    }
    
    public void llenarComboOpciones(){
        cboOpciones.getItems().addAll("Hotel","Fechas","Habitacion","Cliente");
    }
    
    public void mostrarDatos(){
        String c=(String)cboOpciones.getValue();
        if (c.length()!=0){
        if(cboOpciones.getValue().equals("Hotel")){
            for(ReservaRealizada r:reservasReal){
                Hotel h=(Hotel)r.getHot();
                muestraDatos.setText(h+": "+r.getCli()+", "+r.getFechaR()+", "+r.getFechaF()+", "+r.getHa()+", Precio:"+r.getHa().getTarifaSencilla()+",  Comsion de la agencia:"+r.getHa().getTarifaSencilla()*0.10+".\n");
                muestraDatos.setWrapText(true); 
                VBox datos=new VBox();
                datos.getChildren().add(muestraDatos);
                root.setRight(datos);
                
            }
        }else if (cboOpciones.getValue().equals("Fechas")){
            for(ReservaRealizada r:reservasReal){
                LocalDate h=r.getFechaR();
                muestraDatos.setText(h+": "+r.getCli()+", "+r.getHot()+", "+r.getFechaF()+", "+r.getHa()+", Precio:"+r.getHa().getTarifaSencilla()+",  Comsion de la agencia:"+r.getHa().getTarifaSencilla()*0.10+".\n");
                muestraDatos.setWrapText(true); 
                VBox datos=new VBox();
                datos.getChildren().add(muestraDatos);
                root.setRight(datos);
            }
        }else if (cboOpciones.getValue().equals("Habitacion")){
            for(ReservaRealizada r:reservasReal){
                Habitacion h=r.getHa();
                muestraDatos.setText(h+": "+r.getCli()+", "+r.getHot()+", "+r.getFechaF()+", Precio:"+r.getHa().getTarifaSencilla()+",  Comsion de la agencia:"+r.getHa().getTarifaSencilla()*0.10+".\n");
                muestraDatos.setWrapText(true); 
                VBox datos=new VBox();
                datos.getChildren().add(muestraDatos);
                root.setRight(datos);
            }
        }else if (cboOpciones.getValue().equals("Clientes")){
            for(ReservaRealizada r:reservasReal){
                Cliente h=r.getCli();
                muestraDatos.setText(h+": "+", "+r.getHot()+", "+r.getFechaF()+", Precio:"+r.getHa().getTarifaSencilla()+",  Comsion de la agencia:"+r.getHa().getTarifaSencilla()*0.10+".\n");
                muestraDatos.setWrapText(true); 
                VBox datos=new VBox();
                datos.getChildren().add(muestraDatos);
                root.setRight(datos);
            }
        }
        }
        else if (c.length()==0){
            confirm.setDisable(true);
        }
    }
    
    public static BorderPane getRoot() {
        return root;
    }
    
    
}
