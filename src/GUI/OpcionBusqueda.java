/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import agenciaturistica_clicktours.AgenciaTuristica_ClickTours;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author josue
 */
public class OpcionBusqueda {
    private  StackPane root=new StackPane();
    private Button provCiu= new Button("Provincia y Ciudad");
    private Button nomServ= new Button("Nombre hotel y servicio");
    private Button proximidad= new Button("Por proximidad");
    private Button atras= new Button("Atras");
    private StackPane fondo= new StackPane();
    
    public OpcionBusqueda(){
        generarVentana();
    }
    
    public void generarVentana(){
        fondo.setStyle( "-fx-background-image:url('/recursos/Suite.jpg');"+
                         "-fx-background-repeat: stretch;"+
                         "-fx-background-position: center center;");
        
        
        
        HBox cont=new HBox(15);
        cont.setAlignment(Pos.CENTER);
        
        
        cont.getChildren().addAll(provCiu,nomServ,proximidad);
        root.setAlignment(fondo,Pos.CENTER);
        root.setAlignment(atras, Pos.BOTTOM_CENTER);
        root.getChildren().addAll(fondo,atras);
        
        fondo.getChildren().add(cont);
        fondo.setAlignment(cont,Pos.CENTER);
        
        atras.setOnAction(e->AgenciaTuristica_ClickTours.s.setRoot(VentanaRegistro.root)); 
        provCiu.setOnAction(e-> agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new BusquedaHoteles().getRoot())); 
        nomServ.setOnAction(e-> agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new NombreHotelServicio().getRoot()));
        proximidad.setOnAction(e-> agenciaturistica_clicktours.AgenciaTuristica_ClickTours.s.setRoot(new BusquedaProximidad().getRoot()));
    }
    
    public StackPane getRoot(){ 
        return root;
    }
}
