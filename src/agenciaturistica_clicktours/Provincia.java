/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

/**
 *
 * @author josue
 */
public class Provincia extends Formato_ID_Nombre{
    
    private String descripcion;
    private String region;
    private String web;

    public Provincia(String id, String nombre, String descripcion, String region, String web) {
        super(id, nombre);
        this.descripcion = descripcion;
        this.region = region;
        this.web = web;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getRegion() {
        return region;
    }

    public String getWeb() {
        return web;
    }

    @Override
    public String toString() {
        return super.nombre;
    }

    
}
