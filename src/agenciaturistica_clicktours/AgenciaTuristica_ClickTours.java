/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

import GUI.BusquedaHoteles;
import GUI.VentanaRegistro;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author josue
 */
public class AgenciaTuristica_ClickTours extends Application{
    public static Hotel sleccionado=null;
    public static Cliente select=null;
    public static List<Provincia> provincias = new ArrayList<>();
    public static List<Catalogo> catalogos= new ArrayList<>();
    public static List<Ciudad> ciudades = new ArrayList<>();
    public static List<Habitacion> habitaciones = new ArrayList<>();
    public static List<Hotel> hoteles = new ArrayList<>();
    public static List<Servicio> servicios = new ArrayList<>();
    public static List<Cliente> clientes = new ArrayList <> ();
    public static List<Promocion> promociones = new ArrayList <> ();
    public static List<ReservaRealizada> reservasReal = new ArrayList <> ();
    public static String[] archivos= {"catalogo","ciudades","habitaciones","provincias","servicios","hoteles","promociones"};
    public static Scene s;
    
    
    public static void main(String[] args) {

        
        deserializar();
        deserializarReservas();
        leerArchivo();
        leerPromo();
        launch();
        
        serializar();
        
    }
    
    public static void serializar(){
        try(ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("src/recursos/cli.dat"))){
            o.writeObject(clientes);
        }
        catch(IOException e){
            System.out.println(e);
        }
    }
    
    public static void deserializar(){
        try(ObjectInputStream i=new ObjectInputStream(new FileInputStream("src/recursos/cli.dat"))){
            clientes=(ArrayList)i.readObject();
            System.out.println("Objeto deserializado");
            System.out.println(clientes);
        }
        catch(IOException e){
            System.out.println("Error en deserializar "+e);
        }
        catch(ClassNotFoundException ex){
            System.out.println("Error en deserializar "+ex);
        }
    }
    
    public static void deserializarReservas(){
        try(ObjectInputStream i=new ObjectInputStream(new FileInputStream("src/recursos/reservasHechas.dat"))){
            reservasReal=(ArrayList)i.readObject();
            System.out.println("Objeto deserializado ");
            System.out.println(reservasReal);
        }
        catch(IOException e){
            System.out.println("Error en deserializar "+e);
        }
        catch(ClassNotFoundException ex){
            System.out.println("Error en deserializar "+ex);
        }
    }
 
 
    public void start(Stage primaryStage) {
        s= new Scene(new VentanaRegistro().getRoot(),1064,600);
        primaryStage.setTitle("Agencia Turistica Click Tours");
        primaryStage.setScene(s);
        primaryStage.show();
    }
    
    public static void leerArchivo(){
        try{
         for(int i=0; i<archivos.length;i++){
             BufferedReader r=new BufferedReader(new FileReader("src/recursos/"+archivos[i]+".csv"));
             String[] l=r.readLine().split("\\|");
             String linea = " ";
             
             
             if(l[0].equals("id_servicio")){
                 while((linea=r.readLine())!=null){
                     l=linea.split("\\|");
                     Catalogo ca=new Catalogo(l[0],l[1]);
                     catalogos.add(ca);
                 }
             }
             else if(l[0].equals("id_ciudad")){
                 while((linea=r.readLine())!=null){
                     l=linea.split("\\|");
                     Ciudad c= new Ciudad(l[0],l[1],l[2]);
                     ciudades.add(c);
                 }
             }
             
             else if(l[0].equals("﻿id_hotel")){
                 
                 while((linea=r.readLine())!=null){
                     try{
                        l=linea.split("\\|");
                        Double d1=Double.parseDouble(l[10]);
                        Double d2= Double.parseDouble(l[11]);
                        Hotel ho= new Hotel(l[0],l[1],l[2],l[3],l[4],l[5],l[6],l[7],l[8],l[9],d1,d2);
                        hoteles.add(ho);
                        
                     }
                     catch(NumberFormatException e){
                        Hotel ho= new Hotel(l[0],l[1],l[2],l[3],l[4],l[5],l[6],l[7],l[8],l[9],0,0);
                        hoteles.add(ho);
                     }
                 }
             }
             else if(l[0].equals("id_habitacion")){
                 while((linea=r.readLine())!=null){
                     l=linea.split("\\|");
                     Habitacion h= new Habitacion(l[0],l[1],l[2],Double.parseDouble(l[3]),Double.parseDouble(l[4]),Double.parseDouble(l[5]));
                     habitaciones.add(h);
                 }
             }
             else if(l[0].equals("id_Provincia")){
                 
                 while((linea=r.readLine())!=null){
                     l=linea.split("\\|");
                     Provincia p= new Provincia(l[0],l[1],l[2],l[3],l[4]);
                     provincias.add(p);
                 }
             }
             else if(l[0].equals("secuencia")){
                 
                 while((linea=r.readLine())!=null){
                     l=linea.split("\\|");
                     Servicio s= new Servicio(l[0],l[1],l[2],l[3]);
                     servicios.add(s);
                 }
             }
             
         }
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("No se encuentra el indice: "+e);
        }
        catch (FileNotFoundException e){
            System.out.println("El archivo no existe: "+e);
        }
        catch (IOException e){
            System.out.println("Error al leer Archivo: "+e);
        }
    }
    
    public static void leerPromo(){
        try{
            BufferedReader r=new BufferedReader(new FileReader("src/recursos/promociones.csv"));
             String[] l=r.readLine().split("\\|");
             String linea = " ";     
            while((linea=r.readLine())!=null){
                 l=linea.split("\\|");
                 Promocion p= new Promocion(l[0],l[1]);
                 promociones.add(p);
                 }
             }
        catch(ArrayIndexOutOfBoundsException|FileNotFoundException e){
            System.out.println("No se encuentra el archivo el indice no existe "+ e.getMessage());
                }
        catch(IOException e){
            System.out.println("Error al leer Archivo: "+e);
        }
    }
    
}
