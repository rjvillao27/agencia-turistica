/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

/**
 *
 * @author josue
 */
public class Habitacion{
    private String idhab;
    private String idHotel;
    private String nombre;
    private double tarifaSencilla;
    private double tarifaDoble;
    private double tarifaTriple;

    public Habitacion(String idhab, String idHotel, String nombre, double tarifaSencilla, double tarifaDoble, double tarifaTriple) {
        this.idhab=idhab;
        this.idHotel = idHotel;
        this.nombre=nombre;
        this.tarifaSencilla = tarifaSencilla;
        this.tarifaDoble = tarifaDoble;
        this.tarifaTriple = tarifaTriple;
    }

    public String getIdHotel() {
        return idHotel;
    }

    public double getTarifaSencilla() {
        return tarifaSencilla;
    }

    public double getTarifaDoble() {
        return tarifaDoble;
    }

    public double getTarifaTriple() {
        return tarifaTriple;
    }

    @Override
    public String toString() {
        return nombre;
    }

    
}
