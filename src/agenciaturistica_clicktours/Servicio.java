/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

/**
 *
 * @author josue
 */
public class Servicio {
    private String secuencia;
    private String idHotel;
    private String idServicio;
    private String estado;

    public Servicio(String secuencia, String idHotel, String idServicio, String estado) {
        this.secuencia = secuencia;
        this.idHotel = idHotel;
        this.idServicio = idServicio;
        this.estado = estado;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public String getIdHotel() {
        return idHotel;
    }

    public String getIdServicio() {
        return idServicio;
    }

    public String isEstado() {
        return estado;
    }

    @Override
    public String toString() {
        return "Servicio{" + "secuencia=" + secuencia + ", idHotel=" + idHotel + ", idServicio=" + idServicio + ", estado=" + estado + '}';
    }
    
    
}
