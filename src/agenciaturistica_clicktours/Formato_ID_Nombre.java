/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

/**
 *
 * @author anthony
 */
public abstract class Formato_ID_Nombre {
    String id;
    String nombre;

    public Formato_ID_Nombre(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
    
    
}