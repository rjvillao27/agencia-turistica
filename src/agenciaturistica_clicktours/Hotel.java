/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

/**
 *
 * @author josue
 */
public class Hotel extends Formato_ID_Nombre{
    
    private String idCiudad;
    private String descripcionHotel;
    private String tarjetaHotel;
    private String ubicacionHotel;
    private String direccionHotel;
    private String webHotel;
    private String clasificacionHotel;
    private String fotoHotel;
    private double latitud;
    private double longitud;

    public Hotel(String id, String idCiudad, String nombre, String descripcionHotel, String tarjetaHotel, String ubicacionHotel, String direccionHotel, String webHotel, String clasificacionHotel, String fotoHotel, double latitud, double longitud) {
        super(id, nombre);
        this.idCiudad = idCiudad;
        this.descripcionHotel = descripcionHotel;
        this.tarjetaHotel = tarjetaHotel;
        this.ubicacionHotel = ubicacionHotel;
        this.direccionHotel = direccionHotel;
        this.webHotel = webHotel;
        this.clasificacionHotel = clasificacionHotel;
        this.fotoHotel = fotoHotel;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public String getDescripcionHotel() {
        return descripcionHotel;
    }

    public String getTarjetaHotel() {
        return tarjetaHotel;
    }

    public String getUbicacionHotel() {
        return ubicacionHotel;
    }

    public String getDireccionHotel() {
        return direccionHotel;
    }

    public String getWebHotel() {
        return webHotel;
    }

    public String getClasificacionHotel() {
        return clasificacionHotel;
    }

    public String getFotoHotel() {
        return fotoHotel;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    @Override
    public String toString() {
        return nombre;
    }

   

   
}
