/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author josue
 */
public class ReservaRealizada implements Serializable {
    private Cliente cli;
    private Hotel hot;
    private Habitacion ha;
    private LocalDate fechaR;
    private LocalDate fechaF;

    public ReservaRealizada(Cliente cli, Hotel hot, Habitacion ha, LocalDate fechaR, LocalDate fechaF) {
        this.cli = cli;
        this.hot = hot;
        this.ha = ha;
        this.fechaR = fechaR;
        this.fechaF = fechaF;
    }

    public Cliente getCli() {
        return cli;
    }

    public Hotel getHot() {
        return hot;
    }

    public Habitacion getHa() {
        return ha;
    }

    public LocalDate getFechaR() {
        return fechaR;
    }

    public LocalDate getFechaF() {
        return fechaF;
    }
    
}
