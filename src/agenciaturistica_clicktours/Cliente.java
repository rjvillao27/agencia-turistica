/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author anthony
 */
public class Cliente implements Serializable{
    private String id;
    private String nombre;
    private String direccion;
    private String correo;

    public Cliente(String id, String nombre, String direccion, String correo ) {
        this.id=id;
        this.nombre=nombre;
        this.direccion = direccion;
        this.correo = correo;
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

   

  
}
