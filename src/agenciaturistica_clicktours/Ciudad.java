/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciaturistica_clicktours;

/**
 *
 * @author josue
 */
public class Ciudad extends Formato_ID_Nombre{
    private String idProvincia;

    public Ciudad(String id, String idProvincia, String nombre) {
        super(id, nombre);
        this.idProvincia = idProvincia;
    }

    public String getIdProvincia() {
        return idProvincia;
    }

    @Override
    public String toString() {
        return super.nombre;
    }
    
}
